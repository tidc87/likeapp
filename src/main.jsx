import * as React from 'react'
import { Home } from '@app/screens'
import { Provider } from 'react-redux'
import { configureStore } from '@app/redux/store'
import { NativeBaseProvider, StatusBar } from 'native-base'
import { PersistGate } from 'redux-persist/integration/react'
import { SafeAreaProvider } from 'react-native-safe-area-context'

const { store, persistor } = configureStore()

const Main = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <NativeBaseProvider>
        <SafeAreaProvider>
          <StatusBar translucent />
          <Home />
        </SafeAreaProvider>
      </NativeBaseProvider>
    </PersistGate>
  </Provider>
)

export default Main
