import {
  INIT_LIKES,
  RESET_LIKES,
  SET_DISLIKE,
  SET_DISLIKE_ALL,
  SET_LIKE,
  SET_LIKE_ALL,
} from '@app/redux/store/constants'

export const initLikes = (ids) => async (dispatch) =>
  dispatch({
    ids,
    type: INIT_LIKES,
  })

export const setLike = (id) => async (dispatch) =>
  dispatch({
    id,
    type: SET_LIKE,
  })

export const setDislike = (id) => async (dispatch) =>
  dispatch({
    id,
    type: SET_DISLIKE,
  })

export const setLikeAll = () => async (dispatch) =>
  dispatch({
    type: SET_LIKE_ALL,
  })

export const setDislikeAll = () => async (dispatch) =>
  dispatch({
    type: SET_DISLIKE_ALL,
  })

export const resetLikes = () => async (dispatch) =>
  dispatch({
    type: RESET_LIKES,
  })
