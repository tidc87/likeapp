import {
  INIT_LIKES,
  RESET_LIKES,
  SET_DISLIKE,
  SET_DISLIKE_ALL,
  SET_LIKE,
  SET_LIKE_ALL,
} from '@app/redux/store/constants'

const initialState = {
  data: {},
}

const likeReducer = (state = initialState, action) => {
  const clonedData = Object.assign({}, state.data)

  if (Object.hasOwnProperty.call(action, 'id') && !Object.keys(clonedData).includes(action.id + '')) {
    clonedData[action.id] = 0
  }

  switch (action.type) {
    case 'persist/REHYDRATE':
      return {
        ...state,
        ...(action.payload?.like ?? {}),
      }

    case INIT_LIKES:
      for (let i = 0; i < action.ids.length; i++) {
        const id = action.ids[i]

        if (!Object.keys(clonedData).includes(id + '')) {
          clonedData[id] = 0
        }
      }

      return {
        ...state,
        data: clonedData,
      }

    case SET_LIKE:
      clonedData[action.id] = clonedData[action.id] + 1

      return {
        ...state,
        data: clonedData,
      }

    case SET_LIKE_ALL:
      for (const key in clonedData) {
        if (Object.hasOwnProperty.call(clonedData, key)) {
          clonedData[key] = clonedData[key] + 1
        }
      }

      return {
        ...state,
        data: clonedData,
      }

    case SET_DISLIKE:
      if (clonedData[action.id] > 0) clonedData[action.id] = clonedData[action.id] - 1

      return {
        ...state,
        data: clonedData,
      }

    case SET_DISLIKE_ALL:
      for (const key in clonedData) {
        if (Object.hasOwnProperty.call(clonedData, key)) {
          if (clonedData[key] > 0) clonedData[key] = clonedData[key] - 1
        }
      }

      return {
        ...state,
        data: clonedData,
      }

    case RESET_LIKES:
      const newLikes = {}

      for (const key in clonedData) {
        if (Object.hasOwnProperty.call(clonedData, key)) {
          newLikes[key] = 0
        }
      }

      return {
        ...state,
        data: newLikes,
      }

    default:
      return {
        ...state,
        data: clonedData,
      }
  }
}

export default likeReducer
