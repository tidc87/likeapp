import { combineReducers } from '@reduxjs/toolkit'

// Reducers
import likeReducer from './like'

export default combineReducers({
  like: likeReducer,
})
