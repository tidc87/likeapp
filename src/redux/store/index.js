import thunkMiddleware from 'redux-thunk'
import { configureStore as CS } from '@reduxjs/toolkit'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'

import rootReducers from '../reducers'

export const configureStore = () => {
  const persistConfig = {
    whitelist: ['like'],
    storage: AsyncStorage,
    key: '@like-app.core',
  }

  const persistedReducer = persistReducer(persistConfig, rootReducers)
  const store = CS({
    reducer: persistedReducer,
    middleware: [thunkMiddleware],
    devTools: process.env.NODE_ENV !== 'production',
  })

  const persistor = persistStore(store)

  const res = {
    store,
    persistor,
  }

  return {
    ...res,
    initialized: (cb = () => {}) => cb(res),
  }
}
