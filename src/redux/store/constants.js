const makeCommonTypes = (type) => ({
  BEGIN: type.concat('_BEGIN'),
  SUCCESS: type.concat('_SUCCESS'),
  FAILURE: type.concat('_FAILURE'),
})

export const INIT_LIKES = 'INIT_LIKES'
export const SET_LIKE = 'SET_LIKE'
export const SET_DISLIKE = 'SET_DISLIKE'
export const RESET_LIKES = 'RESET_LIKES'
export const SET_LIKE_ALL = 'SET_LIKE_ALL'
export const SET_DISLIKE_ALL = 'SET_DISLIKE_ALL'
