/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react-hooks/exhaustive-deps */
import * as LikeActions from '@app/redux/actions/like'
import { connect } from 'react-redux'
import { useEffect, useMemo } from 'react'
import { Card, Toolbar } from '@app/components'
import { bindActionCreators } from '@reduxjs/toolkit'
import { Box, FlatList, HStack, Text } from 'native-base'

const Home = ({ initLikes }) => {
  const arrDummyDataIds = useMemo(() => Array.from(Array(20).keys()).map((i) => i + 10), [])

  useEffect(() => {
    initLikes(arrDummyDataIds)
  }, [initLikes])

  return (
    <>
      <Box safeAreaTop bg="primary.700" />
      <HStack bg="primary.600" px="3" py="3" alignItems="center" justifyContent="center" w="full">
        <HStack alignItems="center">
          <Text color="white" fontSize="20" fontWeight="bold">
            Home
          </Text>
        </HStack>
      </HStack>
      <Toolbar />
      <FlatList
        px={3}
        pt={3}
        data={arrDummyDataIds}
        keyExtractor={(item) => item}
        ItemSeparatorComponent={<Box h={3} />}
        renderItem={({ item }) => <Card item={item} />}
        contentContainerStyle={{ alignItems: 'center' }}
      />
    </>
  )
}

export default connect(null, (dispatch) =>
  bindActionCreators(
    {
      initLikes: LikeActions.initLikes,
      setLike: LikeActions.setLike,
      setDislike: LikeActions.setDislike,
    },
    dispatch
  )
)(Home)
