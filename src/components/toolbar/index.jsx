/* eslint-disable react/react-in-jsx-scope */
import * as LikeActions from '@app/redux/actions/like'
import { connect } from 'react-redux'
import { Button, HStack, Icon } from 'native-base'
import { bindActionCreators } from '@reduxjs/toolkit'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const Toolbar = ({ setLikeAll, setDislikeAll, resetLikes }) => (
  <HStack
    px={5}
    py={3}
    space={1}
    alignItems="center"
    borderBottomWidth={1}
    borderBottomColor="gray.300"
    justifyContent="space-between"
  >
    <Button
      size="sm"
      colorScheme="blue"
      onPress={setLikeAll}
      leftIcon={<Icon as={FontAwesome} name="thumbs-o-up" size="sm" />}
    >
      LIKE ALL
    </Button>
    <Button variant="outline" size="sm" onPress={resetLikes} _text={{ color: 'black' }}>
      RESET ALL
    </Button>
    <Button
      size="sm"
      colorScheme="red"
      onPress={setDislikeAll}
      leftIcon={<Icon as={FontAwesome} name="thumbs-o-down" size="sm" />}
    >
      DISLIKE ALL
    </Button>
  </HStack>
)

const ConnectedToolbar = connect(null, (dispatch) =>
  bindActionCreators(
    {
      setLikeAll: LikeActions.setLikeAll,
      setDislikeAll: LikeActions.setDislikeAll,
      resetLikes: LikeActions.resetLikes,
    },
    dispatch
  )
)(Toolbar)

export { ConnectedToolbar as Toolbar }
