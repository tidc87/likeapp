/* eslint-disable react/react-in-jsx-scope */
import * as LikeActions from '@app/redux/actions/like'
import { connect } from 'react-redux'
import { bindActionCreators } from '@reduxjs/toolkit'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { AspectRatio, Box, Button, HStack, Icon, Image, Stack, Text } from 'native-base'

const Card = ({ likes, item, setLike, setDislike }) => (
  <Box w="100%" alignItems="center">
    <Box rounded="lg" borderWidth="1" overflow="hidden" borderColor="coolGray.200" backgroundColor="gray.50">
      <Box>
        <AspectRatio w="100%" ratio={16 / 9}>
          <Image
            alt="image"
            source={{
              uri: `https://picsum.photos/id/${item}/400/600`,
            }}
          />
        </AspectRatio>
      </Box>
      <Stack p="4" space={3}>
        <HStack alignItems="center" justifyContent="space-between">
          <Box borderColor="gray.300" borderWidth={1} rounded="md" py="1.5" px="2">
            <Text>
              {likes[item] ?? 0} Like{(likes[item] ?? 0) > 1 && 's'}
            </Text>
          </Box>

          <HStack alignItems="center" justifyContent="flex-end" space={1}>
            <Button
              size="sm"
              colorScheme="blue"
              onPress={() => setLike(item)}
              leftIcon={<Icon as={FontAwesome} name="thumbs-o-up" size="sm" />}
            >
              LIKE
            </Button>
            <Button
              size="sm"
              colorScheme="red"
              onPress={() => setDislike(item)}
              leftIcon={<Icon as={FontAwesome} name="thumbs-o-down" size="sm" />}
            >
              DISLIKE
            </Button>
          </HStack>
        </HStack>
      </Stack>
    </Box>
  </Box>
)

const ConnectedCard = connect(
  ({ like: { data } }) => ({ likes: data }),
  (dispatch) =>
    bindActionCreators(
      {
        setLike: LikeActions.setLike,
        setDislike: LikeActions.setDislike,
      },
      dispatch
    )
)(Card)

export { ConnectedCard as Card }
