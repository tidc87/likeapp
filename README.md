# Like App

## Requirements

Node 14 or greater is required. Development for iOS requires a Mac and Xcode 10 or up, and will target iOS 11 and up.

You also need to install the dependencies required by React Native.  
Go to the [React Native environment setup](https://reactnative.dev/docs/environment-setup), then select `React Native CLI Quickstart` tab.  
Follow instructions for your given `development OS` and `target OS`.

## Quick start

To clone this project simply run :

```
git clone https://gitlab.com/tidc87/likeapp
```

Assuming you have all the requirements installed, you can run the project by running:

- `npm start` to start the metro bundler, in a dedicated terminal
- `npm run <platform>` to run the *platform* application (remember to start a simulator or connect a device)
